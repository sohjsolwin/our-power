package com.sohjsolwin.ourpower;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class ShareUtil 
{
    public static void ShareExtra(Context context, String text, Uri image)
    {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);

        sendIntent.putExtra(Intent.EXTRA_TEXT, text);
        if(image != null)
        {
            sendIntent.putExtra(Intent.EXTRA_STREAM, image);
            sendIntent.setType("images/*");
        }
        else {
            sendIntent.setType("text/plain");
        }
        context.startActivity(Intent.createChooser(sendIntent, context.getResources().getText(R.string.send_to)));
    }

    public static void ShareAchievement(Context context, int achievementID)
    {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);

        sendIntent.putExtra(Intent.EXTRA_TEXT, String.format(context.getResources().getText(R.string.share_achievement).toString(), context.getResources().getText(achievementID).toString()));
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent, context.getResources().getText(R.string.send_to)));
    }
}

